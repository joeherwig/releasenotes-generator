# releaseNotes-generator

This repo contains the files which were created to use an adapted agile-template within the TFS (Azure DevOps Server) ALM process to automatically generate release-notes based on the given work items.

It supports the ability to easily generate release-notes without the need to write and format them individually.
The SSRS-Report fetches the information and can be used to export it to a word document. The macro-file can be used within word (or office automation) to add a table of content (TOC) and to remove unwanted hyperlinks to our internal ALM-Application.
